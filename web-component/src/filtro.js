import {LitElement, html, css} from 'lit-element'

class MiFiltro extends LitElement{
    static get styles() {
		return css`
			:host {
				margin:0px;
                padding:0px
			}
            div{
    margin: 6px;
}
label{
    font-size: 20px;
}
input{
    width: 35%;
}
button#buscar{
    margin-left: 20px;
    border-radius: 10px;
    font-size: 15px;
    background: darkcyan;
    color: white;
}
button#limpiar{
    margin-left: 13px;
    border-radius: 10px;
    font-size: 15px;
    background: darkcyan;
    color: white;
}
            `;
    }

    constructor(){
        super();
    }

    limpiarTabla(){
        let inputBuscar = this.shadowRoot.querySelector('#key');
        inputBuscar.value = "";
     }

     buscarEmpleado(){
        let inputBuscar = this.shadowRoot.querySelector('#key');
        let filtro = inputBuscar.value.toUpperCase();
        let tabla = document.querySelector('web-component').shadowRoot.querySelector('#resu tbody');
        let tr =  tabla.querySelectorAll('tr');

        for (let i = 0; i < tr.length; i++) {
            let td = tr[i].getElementsByTagName('td')
            for (let j = 0; j < td.length; j++) {

                let tdata = td[j];
                if (tdata) {
                    if(tdata.innerHTML.toUpperCase().indexOf(filtro) > -1){
                        tr[i].style.display="";
                        break;
                    }else{
                        tr[i].style.display = "none";
                    }
                }
            }
        }
    }

    render (){
        return html`
        <div>
        <label for="buscar">Filtro: </label>
        <input type="text" name="buscar" placeholder="Buscar" id="key">
        <button @click="${this.buscarEmpleado}" id="buscar" >Buscar</button>
        <button @click="${this.limpiarTabla}" id="limpiar" >Limpiar</button>
        </div>
        `
    }
}
customElements.define("mi-filtro", MiFiltro);