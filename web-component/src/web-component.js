import { LitElement, html, css } from "lit-element";
import "../src/filtro.js";
class WebComponent extends LitElement {
  static get styles() {
    return css`
      :host {
        margin: 0px;
        padding: 0px;
      }
      header {
        border-bottom: 1px solid black;
        background: cadetblue;
        font-size: 25px;
      }
      table {
        border: 1px solid black;
      }
      th {
        border-bottom: 3px solid black;
        background: lightskyblue;
        width: 40%;
      }
      td {
        border-bottom: 1px solid black;
        background: lightcyan;
      }
    `;
  }

  constructor() {
    super();
    this.obtenerDatos();
  }
  async obtenerDatos() {
    const url = "http://dummy.restapiexample.com/api/v1/employees";
    try {
      const respuesta = await fetch(url);
      let resultado = await respuesta.json();
      resultado = resultado.data;
      this.pintarTabla(resultado);
    } catch (error) {
      console.log(error);
    }
  }
  pintarTabla(resultado) {
    let td1 = "";
    let nombre = "";
    let salario = "";

    for (let i = 0; i < resultado.length; i++) {
      nombre = resultado[i].employee_name.split(" ");
      salario = resultado[i].employee_salary;
      td1 += `<tr>
                <td>${nombre[0]}</td>
                <td>${nombre[1]}</td>
                <td>${salario}</td>
            </tr>`;
    }
    this.shadowRoot.querySelector("#resu").insertAdjacentHTML("beforeend", td1);
  }

  render() {
    return html`
      <mi-filtro></mi-filtro>
      <table id="resu">
        <thead>
          <tr>
            <th>Nombre</th>
            <th>Apellido</th>
            <th>Amount YEN</th>
          </tr>
        </thead>
      </table>
    `;
  }
}
customElements.define("web-component", WebComponent);
